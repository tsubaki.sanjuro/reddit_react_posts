
export const  getItems = () => {
	fetch("https://www.reddit.com/r/politics.json?limit=100")
		.then(response => response.json())
		.then(({data}) => {
			this.setState({
				items: data.children,
				isLoading: false
			});
		});
};

export const autoRefresh = () => {
	this.setState(
		state => ({
			isAutoRefreshing: !state.isAutoRefreshing
		}),
		() => {
			if (this.state.isAutoRefreshing) {
				this.autoRefreshing = setInterval(() => {
					this.getItems();
				}, 3000);
			} else {
				clearInterval(this.autoRefreshing);
			}

		});
};

export const updateMinComment = (event) => {
	this.setState({
		minComments: Number(event.target.value)
	});
};

export const getItemsByComments = (items, minComments) => {
	return items.filter(item => item.data.num_comments >= minComments)
		.sort((a, b) => b.data.num_comments - a.data.num_comments);
};