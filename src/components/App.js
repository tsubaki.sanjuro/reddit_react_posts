import React from "react";
import {BallPulse} from "react-pure-loaders";

import "./App.css";
import {Item} from "./Item";


export class App extends React.Component {
	constructor() {
		super();
		this.state = {
			items: [],
			isLoading: false,
			isAutoRefreshing: false,
			minComments: 0,
		};
	}

	componentDidMount() {
		this.setState({
			isLoading: true
		});
		this.getItems();
	}

	getItems = () => {
		fetch("https://www.reddit.com/r/politics.json?limit=100")
			.then(response => response.json())
			.then(({data}) => {
				this.setState({
					items: data.children,
					isLoading: false
				});
			});
	};

	autoRefresh = () => {
		this.setState(
			state => ({
				isAutoRefreshing: !state.isAutoRefreshing
			}),
			() => {
				if (this.state.isAutoRefreshing) {
					this.autoRefreshing = setInterval(() => {
						this.getItems();
					}, 3000);
				} else {
					clearInterval(this.autoRefreshing);
				}

			});
	};

	updateMinComment = (event) => {
		this.setState({
			minComments: Number(event.target.value)
		});
	};

	getItemsByComments = (items, minComments) => {
		return items.filter(item => item.data.num_comments >= minComments)
			.sort((a, b) => b.data.num_comments - a.data.num_comments);
	};

	render() {
		const {items, isLoading, isAutoRefreshing, minComments} = this.state;
		const itemsSortByComments = this.getItemsByComments(items, minComments);

		return (
			<div className='reddit'>
				<h1>Top comments</h1>
				<button className={"refresh-btn"}
						onClick={this.autoRefresh}>
					{isAutoRefreshing ? "Stop " : "Start"} autorefresh
				</button>
				<p>Current filtered comments: {minComments}</p>
				<input
					type="range"
					value={minComments}
					min={0}
					max={13000}
					onChange={this.updateMinComment}
					className={'inputRange'}
				/>
				{isLoading
					?
					(<BallPulse color={'#123abc'} loading={this.state.isLoading}/>)
					:
					(itemsSortByComments.length > 0 ? (itemsSortByComments.map(item => <Item key={item.data.id} data={item.data}/>)):<p>There is nothing to show</p>)}

			</div>
		);
	}
}
