import React from 'react';

export class Item extends React.PureComponent {
	render() {
		const {data} = this.props;
		console.log("render");
		return (
			<div key={data.id} className='reddit__thread'>
				{ data.thumbnail !== "self" ? <img src={data.thumbnail} alt="img"/> : <img src="./post.png" alt="img"/>}
				<h3>Title: {data.title}</h3>
				<p>{data.selftext.slice(0,150)}...
					<a href={data.url} target="_blank" rel="noopener noreferrer">Read moar</a></p>
				<p>{data.num_comments} comments</p>
				<a href={`https://www.reddit.com/${data.permalink}`} target="_blank" rel="noopener noreferrer">Link to thread</a>
			</div>
		);
	}

}